#!/usr/bin/env python



import rospy

from controller.supervisor import Supervisor
    
def main():
    rospy.init_node('sedrad-robot', anonymous=False)
    
    supervisor = Supervisor()
    
    rate = rospy.Rate(4)

    while not rospy.is_shutdown():
        supervisor.execute()
        rate.sleep()


if __name__ == '__main__':
    main()

# rosrun teleop_twist_keyboard teleop_twist_keyboard.py /cmd_vel:=/rosbots_robot/cmd_vel