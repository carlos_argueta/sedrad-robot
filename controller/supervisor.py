#!/usr/bin/env python

import math
import rospy
import tf
import tf2_ros
from geometry_msgs.msg import Pose2D, TransformStamped

from robot import Robot
from rc_teleop import RCTeleop
from go_to_goal import GoToGoal
from stop import Stop
from dynamics.differential_drive import DifferentialDrive

class Supervisor:
    def __init__(self):
        rospy.on_shutdown(self.shutdown_cb)

        self.robot_name = rospy.get_name()
        self.robot = Robot()
        
        # Add controllers here, currently we only have RCTeleop, the remote control via keyboard
        self.controllers = {"rc": RCTeleop()}
        self.switch_to_state("rc")

        # Init the differential drive to change from unicycle model velocities to differential drive velocities
        self.dd = DifferentialDrive(self.robot.wheelbase,
                                    self.robot.wheel_radius)

        # Initialize previous wheel encoder ticks
        self.prev_wheel_ticks = None

    def switch_to_state(self, state):
        self.current_state = state
        self.current_controller = self.controllers[self.current_state]

    def execute(self):
        
        # Get commands in unicycle model
        # This gives us the linear and angular velocities
        ctrl_output = self.current_controller.execute()

        # Convert unicycle model commands to differential drive model
        # This is velocities in meters per second
        diff_output = self.dd.uni_to_diff(ctrl_output["v"], ctrl_output["w"])

        if ctrl_output["v"] != 0.0 or ctrl_output["w"] != 0.0:
            rospy.loginfo(rospy.get_caller_id() +
                          " v: " + str(ctrl_output["v"]) +
                          " w: " + str(ctrl_output["w"]))
            rospy.loginfo(rospy.get_caller_id() +
                          " vl: " + str(diff_output["vl"]) +
                          " vr: " + str(diff_output["vr"]))
        
        # Set the wheel speeds
        self.robot.set_wheel_speed(diff_output["vr"], diff_output["vl"])

      
    def shutdown_cb(self):
        rospy.loginfo(rospy.get_caller_id() +
                      " current controller: " + self.current_state)
        self.switch_to_state("stop")
        
        for ctrl in self.controllers.values():
            ctrl.shutdown()

        self.robot.shutdown()


