#!/usr/bin/env python



import rospy

class DifferentialDrive:
    def __init__(self, wheelbase, wheel_radius):
        # In meters per radian
        # L is the radius of the circle drawn from turning one wheel while
        # holding the other one still - happens to also be the wheelbase
        self.wheelbase = wheelbase 
        self.wheel_radius = wheel_radius

    def uni_to_diff(self, v, w):
        '''
        Return meters per sec wheel velocities 
        '''

        # In meters per radian
        L = self.wheelbase
        R = self.wheel_radius
        
        # w is angular velocity counter clockwise - radians/sec
        # v - m/s
        # L - wheelbase - meter/radian
        # R - wheel radius - meter/radian
        # vr_rad - radians/sec - ie 6.28 == 1 rotation per sec
        vr_rad = ((2.0 * v) + (w * L)) / (2.0 * R)
        vl_rad = ((2.0 * v) + (-1.0 * w * L)) / (2.0 * R)

        # (r/s * 2piR) / 2pi == meters per sec rotational speed
        vr = vr_rad * R
        vl = vl_rad * R

        # In m per sec
        return {"vl": vl, "vr": vr}
