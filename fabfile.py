from fabric.api import *
import fabric.contrib.files as fabfiles
from fabric.utils import fastprint

env.shell = '/bin/bash -l -c' 

WS_DIR = "/catkin_ws"

is_debug = False

def _fp(msg):
    fastprint(msg + "\n")

def _pp(msg):
    """ 
    Print then pause
    """
    global is_debug
    
    _fp(msg)
    
    if is_debug:
        programPause = _get_input("Press the <ENTER> key to continue...")

def step_4_setup_mcu_uno_support_part_2():
    _pp("Plug in the UNO board to the RPi's USB port")

    # Installed avrdude cuz  "platformio run -e uno -t upload" was complaining about a missing file
    # see: https://community.platformio.org/t/solved-avrdude-bin-not-found-in-packages-tool-avrdude/5993
    
    # Installed rosseria-arduino but not sure if that solved anything

    # I will have to try the order of things cuz I am not entirely sure what fixed what

    # Finally, uno_ros_serial is just a node, which is in the rosbots_driver, I had to download it and launch it manually.
    # Later you will have a script to launch it

    home_path = "~"
    git_path = home_path + "/gitspace"
    rosbots_path = git_path + "/rosbots_driver"
    pio_path = rosbots_path + "/platformio/rosbots_firmware"


    robot_ws_dir = home_path + WS_DIR

    if not fabfiles.exists(git_path):
        _fp("Did not find rosbots repo, cloning...")
        run("mkdir " + git_path)

    if not fabfiles.exists(rosbots_path):
        with cd(git_path):
            run("git clone https://github.com/ROSbots/rosbots_driver.git")

        _fp("Creating symbolic link to main ros workspace")
        with cd(robot_ws_dir + "/src"):
            if fabfiles.exists("rosbots_driver"):
                run("rm rosbots_driver")
            run("ln -s " + rosbots_path)
    else:
        _fp("Found rosbots repo, just fetching top and rebasing")
        with cd(rosbots_path):
            run("git fetch origin")
            run("git rebase origin/master")

    with cd(pio_path):
        run("platformio run -e uno -t upload")

    _fp("Installing rosserial package")
    sudo("apt-get install ros-melodic-rosserial")

    _fp("Installing diagnostic-msgs package")
    sudo("apt-get install ros-melodic-diagnostic-msgs")

    

    old_shell = env.shell
    env.shell = '/bin/bash -l -c -i'
    
    with cd(robot_ws_dir):
        run("catkin_make")
        run("catkin_make install")
    env.shell = old_shell



def step_3_setup_mcu_uno_support():
    _pp("Plug in the UNO board to the RPi's USB port")

    # So we can access USB serial port
    sudo("usermod -a -G dialout shl")

    # Some requirements
    sudo("pip install -U testresources")
    
    sudo("pip install -U platformio")

    sudo("pip install -U backports.functools_lru_cache")

    _fp("=============")
    _pp("If this is the first time running setup, the next step will most likely fail since you need a reboot to enable the UNO drivers. If it fails, reboot and run this step again.")
    _fp("=============\n")

def step_2_setup_ros_robot_packages():
    
    _pp("After you successfully install ros_com stuff, install some others. This installs geometry_msgs needed for Twist among other types of basic telemetry messages.")

    _fp("Installing teleop-twist-keyboard package")
    run("sudo apt-get install ros-melodic-teleop-twist-keyboard")

    _fp("Installing image-common package")
    run("sudo apt-get install ros-melodic-image-common")
    
    _fp("Installing cv-bridge package")
    run("sudo apt-get install ros-melodic-cv-bridge")

    _fp("Installing image-geometry package")
    run("sudo apt-get install ros-melodic-image-geometry") 

    _fp("Installing vision-opencv package")
    run("sudo apt-get install ros-melodic-vision-opencv")

    _fp("Installing geometry package")
    run("sudo apt-get install ros-melodic-geometry")

    _fp("Installing geometry2 package")
    run("sudo apt-get install ros-melodic-geometry2")



def step_1_setup_ros():
    global WS_DIR
    global INSTALL_DIR

    # Maybe I should add scripts to install ROS here

    # Create a separate robot_catkin_ws outside of core ROS
    home_path = "~"#run("pwd")
    #ws_dir = home_path + WS_DIR
    robot_ws_dir = home_path + WS_DIR
    if not fabfiles.exists(robot_ws_dir):
        _fp("Need to create and init robot catkin workspace")
        run("mkdir -p " + robot_ws_dir + "/src")
        
        old_shell = env.shell
        env.shell = '/bin/bash -l -c -i'
        with cd(robot_ws_dir + "/src"):
            run("catkin_init_workspace")

        with cd(robot_ws_dir):
            run("catkin_make")
            run("catkin_make install")
        env.shell = old_shell
        
        src_cmd = "source " + robot_ws_dir + "/devel/setup.bash"
        if run("grep '" + src_cmd + "' ~/.bashrc", warn_only=True).succeeded:
            _fp("Sourcing of Robot catkin ws env setup.bash is already in your bashrc")
        else:
            _pp("Going to add Robot catkin ws source setup into your bashrc")
            run("echo '" + src_cmd + "\n' >> ~/.bashrc")


            
            
    '''
    #_pp("All ROS components should be compiled and installed. Going to set up init.d to run ROSBots as a service.")

    # Copy over the rosbots init script - which is kicked off by the init.d
    # service framework
    put("./robot_startup.sh", "~/robot_startup.sh")
    run("chmod +x ~/robot_startup.sh")
    put("./robot_shutdown.sh", "~/robot_shutdown.sh")
    run("chmod +x ~/robot_shutdown.sh")

    # Set up and install the init.d service which will fork and call
    # the rosbots startup script above
    put("./robot_service_template.bash", "~/robot_template")
    run("cat robot_template | sed 's/_TEMPLATE_HOME/" + home_path.replace("/", "\/") + "/' | sed 's/_TEMPLATE_WS_PATH/" + ws_dir.replace("/", "\/") + "/' > robot")
    run("rm robot_template")

    sudo("mv robot /etc/init.d/")
    sudo("chown root:root /etc/init.d/robot")
    sudo("chmod 755 /etc/init.d/robot")
    sudo("update-rc.d robot defaults")
    sudo("systemctl daemon-reload")
    sudo("systemctl stop robot")
    sudo("systemctl start robot")

    _fp("To get IP address of Nano, from a linux system - 'arp -a'")
    
    _fp("Done...")
    '''


def main_setup():
    #step_1_setup_ros()
    #step_2_setup_ros_robot_packages()
    #step_3_setup_mcu_uno_support()
    step_4_setup_mcu_uno_support_part_2()
    
