#!/usr/bin/env python
import rospy
from std_msgs.msg import UInt8

import datetime

count = 0

def callback(data):
    global count
    count += 1
    if data.data == 1:
        now = datetime.datetime.now()
        rospy.loginfo(rospy.get_caller_id() + "I crashed at %s %s",now.strftime("%Y-%m-%d %H:%M:%S") ,data.data)
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("collision_status", UInt8, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()