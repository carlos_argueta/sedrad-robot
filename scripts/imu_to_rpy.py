#!/usr/bin/python
# -*- coding: utf-8 -*-

# Start up ROS pieces.
import rospy
import tf

# ROS messages.
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu


class QuatToEuler():
	def __init__(self):
		self.got_new_msg = False
		

		# Create subscribers and publishers.
		sub_imu1  = rospy.Subscriber("imu1", Imu, self.imu1_callback)
		sub_imu2  = rospy.Subscriber("imu2", Imu, self.imu2_callback)
		#sub_odom  = rospy.Subscriber("odom", Odometry, self.odom_callback)
		#pub_euler = rospy.Publisher("euler", Eulers)

		# Main while loop.
		while not rospy.is_shutdown():
			# Publish new data if we got a new message.
			if self.got_new_msg:
			
			    self.got_new_msg = False

	
	# IMU callback function.
	def imu1_callback(self, msg):
		# Convert quaternions to Euler angles.
		(r, p, y) = tf.transformations.euler_from_quaternion([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])
		#print("IMU1","r",r,"p",p,"y",y)

	def imu2_callback(self, msg):
		# Convert quaternions to Euler angles.
		(r, p, y) = tf.transformations.euler_from_quaternion([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])
		print("IMU2","r",r,"p",p,"y",y)

# Main function.    
if __name__ == '__main__':
	# Initialize the node and name it.
	rospy.init_node('quat_to_euler')
	# Go to class functions that do all the heavy lifting. Do error checking.
	try:
		quat_to_euler = QuatToEuler()
	except rospy.ROSInterruptException: pass