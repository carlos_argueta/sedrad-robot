#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Range

ranges = [0,0,0,0,0,0,0,0,0,0,0,0]
lock = False
def sort_ranges():
	global ranges
	global lock

	if not lock:
		lock = True
		print('====================================')

		indices = sorted(range(len(ranges)), key=lambda k: ranges[k])

		for idx in indices:
			print("/ultrasound%d - %f"%(idx, ranges[idx]))

		lock = False

def ultra0_clb(data):
	global ranges
	ranges[0] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra1_clb(data):
	global ranges
	ranges[1] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra2_clb(data):
	global ranges
	ranges[2] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra3_clb(data):
	global ranges
	ranges[3] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra4_clb(data):
	global ranges
	ranges[4] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra5_clb(data):
	global ranges
	ranges[5] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra6_clb(data):
	global ranges
	ranges[6] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra7_clb(data):
	global ranges
	ranges[7] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra8_clb(data):
	global ranges
	ranges[8] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra9_clb(data):
	global ranges
	ranges[9] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra10_clb(data):
	global ranges
	ranges[10] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()

def ultra11_clb(data):
	global ranges
	ranges[11] = data.range
	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.range)
	sort_ranges()
		
def listener():

 
	rospy.init_node('ultrasound_listener', anonymous=True)

	rospy.Subscriber("ultrasound0", Range, ultra0_clb)
	rospy.Subscriber("ultrasound1", Range, ultra1_clb)
	rospy.Subscriber("ultrasound2", Range, ultra2_clb)
	rospy.Subscriber("ultrasound3", Range, ultra3_clb)
	rospy.Subscriber("ultrasound4", Range, ultra4_clb)
	rospy.Subscriber("ultrasound5", Range, ultra5_clb)
	rospy.Subscriber("ultrasound6", Range, ultra6_clb)
	rospy.Subscriber("ultrasound7", Range, ultra7_clb)
	rospy.Subscriber("ultrasound8", Range, ultra8_clb)
	rospy.Subscriber("ultrasound9", Range, ultra9_clb)
	rospy.Subscriber("ultrasound10", Range, ultra10_clb)
	rospy.Subscriber("ultrasound11", Range, ultra11_clb)
	
	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

if __name__ == '__main__':
	listener()