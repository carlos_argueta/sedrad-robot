#!/usr/bin/env python

import rospy

from std_msgs.msg import String
from geometry_msgs.msg import Pose

from rospy_message_converter import json_message_converter


current_pose = 0

def new_goal_callback(data):
	
	current_pose = int(data.data)
	if current_pose < len(my_poses):
		print("Sending pose number ",current_pose+1)
		pose_publisher.publish(my_poses[current_pose])

def load_poses(input_file, waypoint_type):
	my_poses = []

	f = open(input_file, "r")
	
	if waypoint_type == "pose":

		for line in f:
			pose = json_message_converter.convert_json_to_ros_message('geometry_msgs/Pose', line)
			my_poses.append(pose)

	print("Loaded poses")
	print(my_poses)
	return my_poses

if __name__ == '__main__':
	try:
		rospy.init_node('waypoint_sender', anonymous=True)

		input_file = rospy.get_param('~input_file')
		waypoint_type = rospy.get_param('~type')

		my_poses = load_poses(input_file, waypoint_type)
	
		new_goal_sub = rospy.Subscriber('/path_safety/new_goal', String, callback=new_goal_callback)

		pose_publisher = rospy.Publisher('/waypoint_sender/pose',Pose, queue_size=5)
		
		rate = rospy.Rate(1) # 1hz

		
		
		while not rospy.is_shutdown():

			

			rate.sleep()
	 
	except rospy.ROSInterruptException:
		
		os.kill(node_pid, signal.SIGINT)