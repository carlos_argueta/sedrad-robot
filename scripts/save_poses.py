#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import glob
import math

import rospy

from nav_msgs.msg import Odometry
from  geometry_msgs.msg import PoseWithCovarianceStamped
from  std_msgs.msg import String

from rospy_message_converter import json_message_converter

my_previous_pose = None
my_pose = None
my_file = None

output_file = rospy.get_param('/save_poses/output_file')

def save_pose(my_pose):

	pose = my_pose.pose.pose
	json_pose = json_message_converter.convert_ros_message_to_json(pose)
	my_file.write(json_pose+"\n")
	print("New pose saved")

def pose_callback(data):
	global my_pose
	my_pose = data

def odom_callback(data):
	global my_pose
	my_pose = data


def actions_callback(data):
	global my_pose
	if data.data == "collect-point":
		save_pose(my_pose)
		

if __name__ == '__main__':
	try:
		rospy.init_node('save_poses', anonymous=True)
	
		#rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, pose_callback)
		robot_pose_subscriber = rospy.Subscriber("/odom", Odometry, odom_callback)
		rospy.Subscriber("/actions", String, actions_callback)
		
		rate = rospy.Rate(1) # 1hz

		print("Opening file")
		my_file = open(output_file, "w")

		dist_th = 0.5 # Meters
		
		while not rospy.is_shutdown():
			'''
			if my_previous_pose is None and my_pose is not None:
				print("Setting first pose")
				save_pose(my_pose)
				my_previous_pose = my_pose

			elif my_previous_pose is not None:
				x1 = my_previous_pose.pose.pose.position.x
				x2 = my_pose.pose.pose.position.x
				y1 = my_previous_pose.pose.pose.position.y
				y2 = my_pose.pose.pose.position.y

				dist = math.hypot(x2 - x1, y2 - y1)

				if dist >= dist_th:
					save_pose(my_pose)
					my_previous_pose = my_pose

			'''
	
			rate.sleep()
	 
	except rospy.ROSInterruptException:
		print("Closing file")
		my_file.close()
		os.kill(node_pid, signal.SIGINT)
	   