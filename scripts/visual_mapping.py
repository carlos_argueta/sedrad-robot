#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import glob

import math
import numpy as np

import yaml

import rospy
from std_srvs.srv import Empty

from  geometry_msgs.msg import Pose
from  std_msgs.msg import Bool
from tf.transformations import euler_from_quaternion

from datetime import datetime

import subprocess

import cv2
import imutils

import time


my_pose = None
mapping = False
mapping_init = False
map_ready = False

image_topic = rospy.get_param('/visual_mapper/image_topic')
output_path = rospy.get_param('/visual_mapper/output_path')
current_output_path = None

prev_time = None
map_timeout = 5

error_count = 0
error_th = 5

p = None

#rosrun image_view image_saver image:=/zed/rgb/image_rect_color _save_all_image:=false _filename_format:=/home/robot/Pictures/camera_frames/camera.png  __name:=image_saver
def rotatePoint(centerPoint,point,angle):
    """Rotates a point around another centerPoint. Angle is in degrees.
    Rotation is counter-clockwise"""
    angle = math.radians(angle)
    temp_point = point[0]-centerPoint[0] , point[1]-centerPoint[1]
    temp_point = ( temp_point[0]*math.cos(angle)-temp_point[1]*math.sin(angle) , temp_point[0]*math.sin(angle)+temp_point[1]*math.cos(angle))
    temp_point = int(round(temp_point[0]+centerPoint[0])) , int(round(temp_point[1]+centerPoint[1]))
    return temp_point
    
def save_image_client():
    rospy.wait_for_service('/image_saver/save')
    try:
        save_image = rospy.ServiceProxy('/image_saver/save', Empty)
        resp1 = save_image()
        return resp1
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


def combine(datetime_str, x, y, yaw):

	global error_count
	
	# Load the images
	my_map = cv2.imread('%s/map.pgm'%output_path,1)
	my_view = cv2.imread("%s/camera.png"%output_path ,1)

	try: # To avoid the problem with libpng error: Read Error
		h1, w1 = my_map.shape[:2]
		h2, w2 = my_view.shape[:2]
	except AttributeError:
    		print("shape not found")
    		error_count += 1
    		
    		if error_count >= error_th:
    			mapping = False
    			

    		return
    		
    	error_count = 0
	
	# Draw robot position
	stream = open("%s/map.yaml"%output_path, 'r')
        config = yaml.safe_load(stream)
   	
   	# Convert from position to pixels
   	#print("My pose",my_pose.position.x, config['origin'][0], config['resolution'])
   	#print("My pose",type(my_pose.position.x), type(config['origin'][0]), type(config['resolution']))
	pixel_x = int(round((x - config['origin'][0]) / config['resolution']))
	pixel_y = int(round((-y + config['origin'][1]) / config['resolution'])) + h1
	    		
	
	#print("Position in map", pixel_x, pixel_y, yaw)
	
	# Get the head of the arrow representing the robot
	head = rotatePoint([pixel_x,pixel_y],[pixel_x+50,pixel_y], math.degrees(-yaw))
	#print(head)
	#my_map = cv2.circle(my_map, (pixel_x,pixel_y), radius=7, color=(0, 0, 255), thickness=-1)
	
	# Add the arrow representin the robot
	my_map = cv2.arrowedLine(my_map, (pixel_x,pixel_y), head, color=(0, 0, 255), thickness=5)
	
	
	if h1 > h2:
		
		my_view = imutils.resize(my_view, height=h1)
		h2, w2 = my_view.shape[:2]
	else:
		
		my_map = imutils.resize(my_map, height=h2)
		h1, w1 = my_map.shape[:2]
		
	
	#create empty matrix
	vis = np.zeros((max(h1,h2), w1+w2,3), np.uint8)

	#combine 2 images
	vis[:h1, :w1,:3] = my_map
	vis[:h2, w1:w1+w2,:3] = my_view
	
	
	# Save combined image
	cv2.imwrite('%s/vm_%s.png'%(current_output_path,datetime_str), vis)
	
def pose_callback(data):
	global my_pose
    	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.position)
    	my_pose = data

def mapping_callback(data):
	global mapping
    	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.position)
    	mapping = data.data

def visual_mapper():
    	global my_pose
    	global current_output_path
    	global mapping
    	global mapping_init
    	global p
    	global map_ready
    	global prev_time
    	
	if mapping and my_pose is not None:
	    	#rospy.loginfo(rospy.get_caller_id() + "I heard %s", my_pose.position)	
	    	
	    	if not mapping_init:
	    	
		    	# Create output path
		    	now = datetime.now()

			datetime_str = now.strftime("%Y-%m-%d-%H-%M-%S")
		    	current_output_path = output_path + "/" + datetime_str
		    	os.mkdir(current_output_path)
		    	
		    	mapping_init = True
		    	
		    	print("Starting mapping at %s ..."%current_output_path)
	    	
		now = datetime.now()

		datetime_str = now.strftime("%Y-%m-%d-%H:%M:%S")
			
			
		#rospy.loginfo("Generating mappping %s.png"%datetime_str)
		#print("Saving image")
		# Save the camera view
		save_image_client()
		    
		#print("Saving map")
		# Save the current map   
		#os.system("rosrun map_server map_saver -f %s/map"%output_path)
		
		poll = p.poll()
		if poll is not None:
			map_ready = True
			p = subprocess.Popen(["rosrun", "map_server", "map_saver", "-f", "%s/map"%output_path])
		else:
			map_ready = False
			
		
		    	
		#print("Combining images")
		# Get the robot pose in euler angles
		orientation_q = my_pose.orientation
	        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
	        (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
	        #print(roll, pitch, yaw)
	        
	        # Combine the map and the view
	        if map_ready:
			combine(datetime_str, my_pose.position.x, my_pose.position.y, yaw)
			
			prev_time = time.time()
			
		else:
			print("Map is not ready yet")
			
			if prev_time is not None and (time.time() - prev_time) > map_timeout:
				print("Map timeout")
				mapping = False
				prev_time = None
		    	
		#print("Done!")
		
	else:
		mapping_init = False
		if current_output_path:
			print("Turning mapping off")
			video_saver(current_output_path)
		    	current_output_path = None
		    	
def video_saver(current_output_path):
	print("Saving frames to video")
	img_array = []
	for filename in sorted(glob.glob('%s/*.png'%current_output_path)):#, key=os.path.getmtime): 
	    #print(filename)
	    img = cv2.imread(filename)
	    height, width, layers = img.shape
	    size = (width,height)
	    img_array.append(img)

	if img_array:
		out = cv2.VideoWriter('%s/visual_map.avi'%current_output_path,cv2.VideoWriter_fourcc(*'DIVX'), 2, size)
		 
		for i in range(len(img_array)):
		    out.write(img_array[i])
		out.release()    		

if __name__ == '__main__':
    try:
    	rospy.init_node('visual_mapping', anonymous=True)
    
    	rospy.Subscriber("robot_pose", Pose, pose_callback)
    	rospy.Subscriber("/dalu_robot/slam/mapping", Bool, mapping_callback)
    	
    	rate = rospy.Rate(1) # 1hz
    	
    	p = subprocess.Popen(["rosrun", "map_server", "map_saver", "-f", "%s/map"%output_path])
    	# Start the image saver
    	#print("rosrun image_view image_saver image:=%s _save_all_image:=false _filename_format:=%s/camera.png"%(image_topic,output_path))
    	#os.system("rosrun image_view image_saver image:=%s _save_all_image:=false _filename_format:=%s/camera.png"%(image_topic,output_path))
    	#p = subprocess.Popen(["rosrun", "image_view", "image_saver", "image:=%s"%image_topic,  "_save_all_image:=false", "_filename_format:=%s/camera.png"%output_path, "&"], shell=True)
    	#node_pid = os.spawnv(os.P_NOWAIT, "rosrun", ["image_view", "image_saver", "image:=%s"%image_topic,  "_save_all_image:=false", "_filename_format:=%s/camera.png"%output_path])
    	#print(node_pid)
    	while not rospy.is_shutdown():
    
        	visual_mapper()
        	
        	rate.sleep()
     
    except rospy.ROSInterruptException:
    	
	video_saver(current_output_path)

    	#os.kill(node_pid, signal.SIGINT)
        pass